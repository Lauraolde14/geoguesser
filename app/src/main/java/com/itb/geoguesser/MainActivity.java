package com.itb.geoguesser;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

public class MainActivity extends AppCompatActivity {
    TextView textTitle;
    TextView text_pregunta;
    ProgressBar progressBar;
    TextView numero_pregunta;
    Button Resposta1;
    Button Resposta2;
    Button Resposta3;
    Button Resposta4;
    TextView textScore;
    Button hint;

    private Question preguntes = new Question();
    private String respostaCorrecte;
    private int Score = 0;
    private int preguntesLenght  = preguntes.preguntes.length;
    Random r;
    private int Increment = (int) 100 / 10;
    private int numP = 1;
    private int click = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        r = new Random();

        textTitle = findViewById(R.id.text_title);
        text_pregunta = findViewById(R.id.text_pregunta);
        progressBar = findViewById(R.id.progressBar);
        numero_pregunta = findViewById(R.id.numero_pregunta);
        Resposta1 = findViewById(R.id.resposta1);
        Resposta2 = findViewById(R.id.resposta2);
        Resposta3 = findViewById(R.id.resposta3);
        Resposta4 = findViewById(R.id.resposta4);
        textScore = findViewById(R.id.textScore);
        hint = findViewById(R.id.hint);

        textScore.setText("Puntuació: " + Score);
        seguentPregunta(r.nextInt(preguntesLenght));
        progressBar.setProgress(10);

        Resposta1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Resposta1.getText() == respostaCorrecte){
                    preguntaCorrecte();
                }else{
                    preguntaIncorrecte();
                }
            }
        });

        Resposta2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Resposta2.getText() == respostaCorrecte){
                    preguntaCorrecte();
                }else{
                    preguntaIncorrecte();
                }
            }
        });

        Resposta3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Resposta3.getText() == respostaCorrecte){
                    preguntaCorrecte();
                }else{
                    preguntaIncorrecte();
                }
            }
        });

        Resposta4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Resposta4.getText() == respostaCorrecte){
                    preguntaCorrecte();
                }else{
                    preguntaIncorrecte();
                }
            }
        });

        hint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                click++;
                if(click == 3){
                    Toast.makeText(getApplicationContext(), "Resposta: " + respostaCorrecte, Toast.LENGTH_SHORT).show();
                    Toast.makeText(getApplicationContext(), "Ja has utilitzat els 3 intents", Toast.LENGTH_SHORT).show();
                    hint.setVisibility(View.INVISIBLE);
                }else if(click == 1){
                    Toast.makeText(getApplicationContext(), "Aquest botó només es pot utilitzar 3 vegades", Toast.LENGTH_SHORT).show();
                    Toast.makeText(getApplicationContext(), "Resposta: " + respostaCorrecte, Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(getApplicationContext(), "Resposta: " + respostaCorrecte, Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    public void preguntaCorrecte() {
        Score++;
        textScore.setText("Puntuació: " + Score);
        Toast.makeText(getApplicationContext(), "CORRECTE!!", Toast.LENGTH_SHORT).show();
        seguentPregunta(r.nextInt(preguntesLenght));
        if(numP == 11){
            quizAcabat();
        }
    }

    public void preguntaIncorrecte(){
        Toast.makeText(getApplicationContext(), "INCORRECTE! Resposta: " + respostaCorrecte, Toast.LENGTH_SHORT).show();
        seguentPregunta(r.nextInt(preguntesLenght));
        if(numP == 11){
            quizAcabat();
        }
    }

    private void seguentPregunta(int num){
        text_pregunta.setText(preguntes.getQuestion(num));
        Resposta1.setText(preguntes.getResposta1(num));
        Resposta2.setText(preguntes.getResposta2(num));
        Resposta3.setText(preguntes.getResposta3(num));
        Resposta4.setText(preguntes.getResposta4(num));
        numero_pregunta.setText("Pregunta " + numP + " de 10");
        numP++;

        progressBar.incrementProgressBy(Increment);

        respostaCorrecte = preguntes.getCorrectResposta(num);
    }

    private void quizAcabat(){
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle("Felicitats, has acabat el Quiz");
        dialog.setMessage("La teva puntuació ha estat de: " + Score*10 + "/100 punts");

        dialog.setNegativeButton("Finalitzar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });

        dialog.show();
    }
}