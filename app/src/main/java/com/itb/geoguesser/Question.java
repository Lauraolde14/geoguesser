package com.itb.geoguesser;

public class Question {
    public String preguntes[] = {
            "Quina és la capital de Afganistan?",
            "Quina és la capital de Albània?",
            "Quina és la capital de Bielorússia?",
            "Quina és la capital de Grècia?",
            "Quina és la capital de Marroc?",
            "Quina és la capital de Perú?",
            "Quina és la capital de Síria?",
            "Quina és la capital de Uganda?",
            "Quina és la capital de Turquia?",
            "Quina és la capital de Romania?"
    };

    private String respostes[][] = {
            {"Kabul", "Melekeok", "Santo Domingo", "Dakar"},
            {"Assumpció", "Viena", "Tirana", "Gitega"},
            {"Sucre", "Minsk", "Sofia", "Ottawa"},
            {"Moroni", "Doha", "Timbu", "Atenes"},
            {"Acra", "Tallinn", "Rabat", "Nicòsia"},
            {"Lima", "Bisau", "Conakri", "Kingston"},
            {"Kuwait", "Astana", "Tarawa", "Damasc"},
            {"Malé", "Kampala", "Palikir", "Yaren"},
            {"Ankara", "Masqat", "Varsòvia", "Praga"},
            {"Dakar", "Moscou", "Kigali", "Bucarest"}
    };

    private String respostaCorrecte[] = {
            "Kabul",
            "Tirana",
            "Minsk",
            "Atenes",
            "Rabat",
            "Lima",
            "Damasc",
            "Kampala",
            "Ankara",
            "Bucarest"
    };

    public String getQuestion(int a){
        String question = preguntes[a];
        return question;
    }

    public String getResposta1(int a){
        String resposta = respostes[a][0];
        return resposta;
    }

    public String getResposta2(int a){
        String resposta = respostes[a][1];
        return resposta;
    }

    public String getResposta3(int a){
        String resposta = respostes[a][2];
        return resposta;
    }

    public String getResposta4(int a){
        String resposta = respostes[a][3];
        return resposta;
    }

    public String getCorrectResposta(int a){
        String correcte = respostaCorrecte[a];
        return correcte;
    }
}
